
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class Main{

private static ArrayList<Node> nodes = new ArrayList<>();
private static ArrayList<Node> linkedNodes = new ArrayList<>();
private static LinkedList<Road> roads = new LinkedList<>();


public static void main(String[] args)
{
    generateNodes(getRandomNumberInRange(30,60), 50);
    generateRoads();

    BufferedImage res = createMap(2040,2040);
    saveBMP(res, System.getProperty("user.dir")+"/map.png");


}

private static void generateNodes(int size, int spacing)
{

    for(int i = 0; i<=size;i++ )
    {
        int x, y;
        x = getRandomNumberInRange(300,1700);
        y = getRandomNumberInRange(300,1700);

        for(Node n : nodes){
            int orientation = getRandomNumberInRange(0,7);
            while(n.getDistance(n.getXcoord(), n.getYcoord(), x, y) < spacing){
                switch(orientation)
                {
                    case 0: x+=10;
                        break;
                    case 1: x-=10;
                        break;
                    case 2: y+=10;
                        break;
                    case 3: y-=10;
                        break;
                    case 4: x+=10; y+=10;
                        break;
                    case 5: x-=10; y+=10;
                        break;
                    case 6: x-=10; y+=10;
                        break;
                    case 7: x-=10; y-=10;
                        break;
                }System.out.printf(" new pos x: %d,y: %d\n", x,y);
                //System.out.printf("Distance : %f\n", (n.getDistance(n.getXcoord(), n.getYcoord(), x, y)));

            }
        }

        Node newNode = new Node(x,y, Node.nodeType.center);
        nodes.add(newNode);
        System.out.printf("node %d added x: %d, y: %d\n", i, x, y);

    }

    //TODO:bends can connect to other bends or squares
    //
}

    private static void generateRoads()
    {
        Node infNode = new Node(5000,5000, Node.nodeType.center);

        Node currentNode = nodes.get(0);
        while(nodes.size()>2)
        {
            Node nearestNode = infNode;
            for(Node n : nodes)
            {
                if(n!=currentNode && currentNode.getDistance(n) < currentNode.getDistance(nearestNode))
                {
                    nearestNode = n;
                }
            }
            linkedNodes.add(currentNode);
            Road r = new Road(currentNode.getXcoord(), currentNode.getYcoord(),nearestNode.getXcoord(),nearestNode.getYcoord());
            roads.add(r);
            nodes.remove(nodes.indexOf(currentNode));
            currentNode = nearestNode;



        }


        for(Node n : linkedNodes)
        {
            System.out.printf("x : %d, y: %d \n", n.getXcoord(), n.getYcoord());
        }
        for(Road r : roads)
        {
            System.out.printf("start : %d,%d end : %d,%d \n", r.getStartX(), r.getStartY(), r.getEndX(), r.getEndY());
        }

    }


    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static BufferedImage createMap(int sizeX, int sizeY)
    {
        final BufferedImage res = new BufferedImage( sizeX, sizeY, BufferedImage.TYPE_INT_RGB);
        for(int x = 0; x < sizeX; x++)
        {
            for(int y = 0; y < sizeY; y++)
            {
                res.setRGB(x, y, Color.WHITE.getRGB());
            }
        }

        for (Node n : linkedNodes) {
            for (int i = 0; i < 20; i++) {
                res.setRGB(n.getXcoord() + i, n.getYcoord(), Color.BLACK.getRGB());
                res.setRGB(n.getXcoord() - i, n.getYcoord(), Color.BLACK.getRGB());


                for (int j = 0; j < 20; j++) {
                    res.setRGB(n.getXcoord() + i, n.getYcoord() + j, Color.BLACK.getRGB());
                    res.setRGB(n.getXcoord() + i, n.getYcoord() - j, Color.BLACK.getRGB());

                    res.setRGB(n.getXcoord() - i, n.getYcoord() + j, Color.BLACK.getRGB());
                    res.setRGB(n.getXcoord() - i, n.getYcoord() - j, Color.BLACK.getRGB());
                }

            }

        }
        for (Road r : roads) {
        for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y< sizeY; y++)

                if (isInLine(x, y, r)) {

                        res.setRGB(x, y, Color.BLACK.getRGB());


                }
            }
        }



        return res;

    }

    public static void saveBMP(final BufferedImage bi, final String path)
    {
        try {
            RenderedImage ri = bi;
            ImageIO.write(ri, "png", new File(path));
        }
        catch(IOException e){
            e.printStackTrace();
        }

    }

    public static boolean isInLine(int x, int y, Road r)
    {

        // if distance(A, C) + distance(B, C) == distance(A, B);
         if(Math.abs(Math.sqrt(Math.pow(x-r.getStartX(),2) + Math.pow(y-r.getStartY(),2)) + Math.sqrt(Math.pow(x-r.getEndX(),2) + Math.pow(y-r.getEndY(),2)) - Math.sqrt(Math.pow(r.getStartX()-r.getEndX(),2) + Math.pow(r.getStartY()-r.getEndY(),2))) <1)
         {
             return true;

         }
         return false;


    }



}
