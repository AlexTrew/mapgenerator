import java.util.ArrayList;
import java.lang.Math;
public class Node{

    public enum nodeType{
        plaza,crossroad,bend,center
    }

    private int xcoord;
    private int ycoord;
    private int size;
    private int level;
    private boolean reached = false;

    private ArrayList<Road> connections = new ArrayList<>();

    private nodeType nt;




    public Node(int xcoord, int ycoord,nodeType nt)
    {
	    this.xcoord = xcoord;
	    this.ycoord = ycoord;
	    this.nt = nt;

    }

    public int getXcoord()
    {
        return xcoord;
    }

    public int getYcoord()
    {
        return ycoord;
    }

    public double getDistance(Node n)
    {
        return Math.sqrt(Math.pow(getXcoord()-n.getXcoord(),2) + Math.pow(getYcoord()-n.getYcoord(),2));
    }

    public double getDistance(int x1, int y1, int x2, int y2)
    {
        return Math.sqrt(Math.pow(x1-x2,2) + Math.pow(y1-y2,2));
    }

    public boolean getReached()
    {
        return reached;
    }

    public void setReached(boolean r)
    {
        reached = r;
    }




}
